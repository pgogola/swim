package com.example.piotrgogola.lab3home;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements ActionBar.TabListener {

    private static final String TAB_TAG1 = "TabTag1";
    private static final String TAB_TAG2 = "TabTag2";
    private static final String TAB_TAG3 = "TabTag3";
    private static final String TAB_TAG4 = "TabTag4";

    private MyData myData;
    private InputDataSelect inputDataSelect;

    private Data1List data1List;
    private Data2List data2List;

    private ActionBar.Tab tab1;
    private ActionBar.Tab tab2;
    private ActionBar.Tab tab3;
    private ActionBar.Tab tab4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        /*
        fragmenty do przelaczenia pomiedzy zakladkami
         */
            myData = new MyData();
            inputDataSelect = new InputDataSelect();
            data1List = new Data1List();
            data2List = new Data2List();
            transaction.add(R.id.mainKontener, myData, TAB_TAG1);
            transaction.detach(myData);
            transaction.add(R.id.mainKontener, inputDataSelect, TAB_TAG2);
            transaction.detach(inputDataSelect);
            transaction.add(R.id.mainKontener, data1List, TAB_TAG3);
            transaction.detach(data1List);
            transaction.add(R.id.mainKontener, data2List, TAB_TAG4);
            transaction.detach(data2List);
        /*
        koniec transakcji
         */
            transaction.commit();

            ActionBar actionBar = getSupportActionBar();
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
            tab1 = actionBar.newTab().setText("1");
            tab1.setTabListener(this);
            tab2 = actionBar.newTab().setText("2");
            tab2.setTabListener(this);
            tab3 = actionBar.newTab().setText("3");
            tab3.setTabListener(this);
            tab4 = actionBar.newTab().setText("4");
            tab4.setTabListener(this);
            actionBar.addTab(tab1, true);
            actionBar.addTab(tab2, false);
            actionBar.addTab(tab3, false);
            actionBar.addTab(tab4, false);
        }
        else
        {
            ActionBar actionBar = getSupportActionBar();
            myData = (MyData) getSupportFragmentManager().findFragmentByTag(TAB_TAG1);
            inputDataSelect = (InputDataSelect) getSupportFragmentManager().findFragmentByTag(TAB_TAG2);;
            data1List = (Data1List) getSupportFragmentManager().findFragmentByTag(TAB_TAG3);;
            data2List = (Data2List)getSupportFragmentManager().findFragmentByTag(TAB_TAG4);;
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        }
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (tab.getPosition()) {
            case 0:
                transaction.attach(myData);
                break;
            case 1:
                transaction.attach(inputDataSelect);
                break;
            case 2:
                transaction.attach(data1List);
                break;
            case 3:
                transaction.attach(data2List);
                break;
        }
        transaction.commit();
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (tab.getPosition()) {
            case 0:
                transaction.detach(myData);
                break;
            case 1:
                transaction.detach(inputDataSelect);
                break;
            case 2:
                transaction.detach(data1List);
                break;
            case 3:
                transaction.detach(data2List);
                break;
        }
        transaction.commit();
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }
}
