package com.example.piotrgogola.lab3home;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Piotr Gogola on 09.04.2018.
 */

public class MyCarAdapter extends BaseAdapter {

    private class CarRecordRow {
        TextView carName;
    }

    static public ArrayList<CarRecord> list2content = new ArrayList<>();

    private int layoutResource;

    private Context context;

    public MyCarAdapter(Context context, int layoutResource)
    {
        super();
        this.context = context;
        this.layoutResource = layoutResource;
    }

    @Override
    public int getCount() {
        return list2content.size();
    }

    @Override
    public Object getItem(int i) {
        return list2content.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            view = layoutInflater.inflate(layoutResource, null);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CarInfo.class);
                intent.putExtra("id", i);
                context.startActivity(intent);
            }
        });

        CarRecord carRecord = (CarRecord) getItem(i);
        CarRecordRow carRecordRow;
        if(carRecord != null)
        {
            carRecordRow = new CarRecordRow();
            carRecordRow.carName = view.findViewById(R.id.carRowName);
            view.setTag(carRecordRow);
        }
        else
        {
            carRecordRow = (CarRecordRow) view.getTag();
        }

        carRecordRow.carName.setText(carRecord.getCarType());
        carRecordRow.carName.setTextColor(carRecord.getColor());

        return view;
    }
}
