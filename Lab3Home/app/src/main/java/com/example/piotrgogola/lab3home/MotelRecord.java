package com.example.piotrgogola.lab3home;

import android.widget.CheckBox;
import android.widget.RatingBar;
import android.widget.TextView;


/**
 * Created by Piotr Gogola on 09.04.2018.
 */

public class MotelRecord {
    private String motelName;
    private String roomType;
    private float ratting;
    private boolean breakfast;
    private boolean dinner;
    private boolean supper;

    MotelRecord(String motelName, String roomType, float ratting, boolean breakfast, boolean dinner, boolean supper) {
        this.motelName = motelName;
        this.roomType = roomType;
        this.ratting = ratting;
        this.breakfast = breakfast;
        this.dinner = dinner;
        this.supper = supper;
    }

    public String getMotelName() {
        return motelName;
    }

    public String getRoomType() {
        return roomType;
    }

    public float getRatting() {
        return ratting;
    }

    public boolean getBreakfast() {
        return breakfast;
    }

    public boolean getDinner() {
        return dinner;
    }

    public boolean getSupper() {
        return supper;
    }
}
