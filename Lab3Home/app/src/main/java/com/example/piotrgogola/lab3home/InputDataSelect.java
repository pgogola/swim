package com.example.piotrgogola.lab3home;


import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;


interface DataTypeSelectListener {
    void onDataTypeSelect(int option);
}

/**
 * A simple {@link Fragment} subclass.
 */
public class InputDataSelect extends Fragment implements RadioGroup.OnCheckedChangeListener, DataTypeSelectListener {

    private InputData1 inputData1;
    private InputData2 inputData2;

    private LayoutInflater inflater;
    private ViewGroup container;

    private static final String TAG_1 = "Input1";
    private static final String TAG_2 = "Input2";

    public InputDataSelect() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.inflater = inflater;
        this.container = container;
        return inflater.inflate(R.layout.fragment_input_data_select, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
            ((RadioGroup) getActivity().findViewById(R.id.selectRadioGroup)).setOnCheckedChangeListener(this);

            RadioButton radioButton = getActivity().findViewById(R.id.motelRadioButton);
            radioButton.setChecked(true);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {

            inputData1 = new InputData1();
            inputData2 = new InputData2();
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            /*
            fragmenty do wyboru opcji
            */
            transaction.add(R.id.kontenerWybierz, inputData1, TAG_1);
            transaction.detach(inputData1);
            transaction.add(R.id.kontenerWybierz, inputData2, TAG_2);
            transaction.detach(inputData2);
            transaction.attach(inputData1).commit();
        } else {
            inputData1 = (InputData1) getChildFragmentManager().findFragmentByTag(TAG_1);
            inputData2 = (InputData2) getChildFragmentManager().findFragmentByTag(TAG_2);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        onDataTypeSelect(i);
    }

    @Override
    public void onDataTypeSelect(int option) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        switch (option) {
            case R.id.motelRadioButton:
                transaction.detach(inputData2);
                transaction.attach(inputData1);
                break;
            case R.id.carRadioButton:
                transaction.detach(inputData1);
                transaction.attach(inputData2);
                break;
        }
        transaction.commit();
    }
}
