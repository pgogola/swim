package com.example.piotrgogola.lab3home;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Piotr Gogola on 09.04.2018.
 */

public class MyMotelAdapter extends BaseAdapter {

    private class MotelRecordRow {
        TextView motelName;
    }

    static public ArrayList<MotelRecord> list1content = new ArrayList<>();

    private int layoutResource;

    private Context context;

    public MyMotelAdapter(Context context, int layoutResource)
    {
        super();
        this.context = context;
        this.layoutResource = layoutResource;
    }

    @Override
    public int getCount() {
        return list1content.size();
    }

    @Override
    public Object getItem(int i) {
        return list1content.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            view = layoutInflater.inflate(layoutResource, null);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MotelInfo.class);
                intent.putExtra("id", i);
                context.startActivity(intent);
            }
        });


        MotelRecord motelRecord = (MotelRecord) getItem(i);
        MotelRecordRow motelRecordRow;
        if(motelRecord != null)
        {
            motelRecordRow = new MotelRecordRow();
            motelRecordRow.motelName = view.findViewById(R.id.listRowName);
            view.setTag(motelRecordRow);
        }
        else
        {
            motelRecordRow = (MotelRecordRow) view.getTag();
        }

        motelRecordRow.motelName.setText(motelRecord.getMotelName());

        return view;
    }
}
