package com.example.piotrgogola.lab3home;


import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class InputData2 extends Fragment {

    private EditText carType;
    private EditText carModel;
    private SeekBar colorR;
    private SeekBar colorG;
    private SeekBar colorB;
    private RadioGroup fuelType;


    public InputData2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setRetainInstance(true);
        return inflater.inflate(R.layout.fragment_input_data2, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        carType = getActivity().findViewById(R.id.carType);
        carModel = getActivity().findViewById(R.id.carModel);
        colorR = getActivity().findViewById(R.id.seekBarR);
        colorG = getActivity().findViewById(R.id.seekBarG);
        colorB = getActivity().findViewById(R.id.seekBarB);
        fuelType = getActivity().findViewById(R.id.fuelTypeRadioGroup);

        RadioButton dieselButton = getActivity().findViewById(R.id.dieselRadioButton);
        dieselButton.setChecked(true);

        getActivity().findViewById(R.id.data2SaveOnClick).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(carModel.getText().toString().isEmpty() || carType.getText().toString().isEmpty())
                {
                    Toast.makeText(getContext(), "Uzupełnij wszystkie pola", Toast.LENGTH_SHORT).show();
                }
                else {
                    RadioButton fuel = getActivity().findViewById(fuelType.getCheckedRadioButtonId());
                    CarRecord carRecord = new CarRecord(carType.getText().toString(), carModel.getText().toString(),
                            fuel.getText().toString(), (colorR.getProgress() << 16)
                            | (colorG.getProgress() << 8) | colorB.getProgress() | 0xff000000);
                    MyCarAdapter.list2content.add(carRecord);
                }
            }
        });

        getActivity().findViewById(R.id.data2ResetOnClick).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                carType.setText("");
                carModel.setText("");
                RadioButton dieselButton = getActivity().findViewById(R.id.dieselRadioButton);
                dieselButton.setChecked(true);
                colorR.setProgress(0);
                colorG.setProgress(0);
                colorB.setProgress(0);
            }
        });
    }
}
