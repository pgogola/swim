package com.example.piotrgogola.lab3home;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class InputData1 extends Fragment implements AdapterView.OnItemSelectedListener {

    private final static String[] roomsTypeArray = {"Jednoosobowy", "Dwuosobowy", "Trzyosobowy", "Czteroosobowy"};

    private CheckBox breakfastOption;
    private CheckBox dinnerOption;
    private CheckBox supperOption;
    private RatingBar ratingBar;
    private EditText motelName;
    private Spinner roomType;

    public InputData1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setRetainInstance(true);
        return inflater.inflate(R.layout.fragment_input_data1, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        breakfastOption = (CheckBox) getActivity().findViewById(R.id.breakfastCheckBox);
        dinnerOption = (CheckBox) getActivity().findViewById(R.id.dinnerCheckBox);
        supperOption = (CheckBox) getActivity().findViewById(R.id.supperCheckBox);
        ratingBar = (RatingBar) getActivity().findViewById(R.id.ratingBar);
        motelName = (EditText) getActivity().findViewById(R.id.hotelName);
        roomType = (Spinner) getActivity().findViewById(R.id.roomType);

        roomType.setOnItemSelectedListener(this);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, roomsTypeArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        roomType.setAdapter(adapter);
        ratingBar.setRating(2.5f);
        roomType.setSelection(1);

        /*
        Słuchacze przycisków
         */
        ((Button) getActivity().findViewById(R.id.data1SaveOnClick)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(motelName.getText().toString().isEmpty())
                {
                    Toast.makeText(getContext(), "Uzupełnij wszystkie pola", Toast.LENGTH_SHORT).show();
                }
                else {
                    MotelRecord motelRecord = new MotelRecord(motelName.getText().toString(),
                            roomType.getSelectedItem().toString(), ratingBar.getRating(),
                            breakfastOption.isChecked(), dinnerOption.isChecked(),
                            supperOption.isChecked());
                    MyMotelAdapter.list1content.add(motelRecord);
                }
            }
        });

        ((Button) getActivity().findViewById(R.id.data1ResetOnClick)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                breakfastOption.setChecked(false);
                dinnerOption.setChecked(false);
                supperOption.setChecked(false);
                ratingBar.setRating(2.5f);
                motelName.setText("");
                roomType.setSelection(1);
            }
        });


    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
