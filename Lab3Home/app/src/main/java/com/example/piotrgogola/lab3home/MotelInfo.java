package com.example.piotrgogola.lab3home;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RatingBar;
import android.widget.TextView;

public class MotelInfo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motel_info);

        MotelRecord motelRecord = MyMotelAdapter.list1content.get(getIntent().getIntExtra("id", -1));

        ((TextView)findViewById(R.id.hotelNameInfo)).setText(motelRecord.getMotelName());
        ((TextView)findViewById(R.id.roomTypeInfo)).setText(motelRecord.getRoomType());
        ((RatingBar)findViewById(R.id.ratingBarInfo)).setRating(motelRecord.getRatting());
        ((CheckBox)findViewById(R.id.breakfastCheckBoxInfo)).setChecked(motelRecord.getBreakfast());
        ((CheckBox)findViewById(R.id.dinnerCheckBoxInfo)).setChecked(motelRecord.getDinner());
        ((CheckBox)findViewById(R.id.supperCheckBoxInfo)).setChecked(motelRecord.getSupper());

    }

    public void backButton1OnClick(View view) {
        onBackPressed();
    }
}
