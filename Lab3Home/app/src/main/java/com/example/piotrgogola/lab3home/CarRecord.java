package com.example.piotrgogola.lab3home;

import android.graphics.drawable.ColorDrawable;

/**
 * Created by Piotr Gogola on 09.04.2018.
 */

public class CarRecord {
    private String carType;
    private String carModel;
    private String fuel;
    private int color;

    CarRecord(String carType, String carModel, String fuel, int color)
    {
        this.carType = carType;
        this.carModel = carModel;
        this.fuel = fuel;
        this.color = color;
    }


    public String getCarType() {
        return carType;
    }

    public String getCarModel() {
        return carModel;
    }

    public String getFuel() {
        return fuel;
    }

    public int getColor() {
        return color;
    }
}
