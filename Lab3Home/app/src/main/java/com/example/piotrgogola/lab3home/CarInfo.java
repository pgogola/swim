package com.example.piotrgogola.lab3home;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class CarInfo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_info);


        CarRecord carRecord = MyCarAdapter.list2content.get(getIntent().getIntExtra("id", -1));

        ((TextView)findViewById(R.id.carTypeInfo)).setText(carRecord.getCarType());
        ((TextView)findViewById(R.id.carModelInfo)).setText(carRecord.getCarModel());
        ((TextView)findViewById(R.id.carFuelInfo)).setText(carRecord.getFuel());
        ((View)findViewById(R.id.carColorInfo)).setBackgroundColor(carRecord.getColor());


    }

    public void backButton2OnClick(View view) {
        onBackPressed();
    }
}
