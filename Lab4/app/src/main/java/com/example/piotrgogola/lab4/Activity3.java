package com.example.piotrgogola.lab4;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Hashtable;
import java.util.Map;

public class Activity3 extends Activity {

    private boolean text3IsUnderline;
    private boolean text3IsBold;
    private boolean text3IsItalic;
    private static Map<String, String> format = new Hashtable<>(3);

    private ImageView image;
    static {
        format.put("underline", "<u></u>");
        format.put("bold", "<b></b>");
        format.put("italic", "<i></i>");
    }

    @NonNull
    private String formatText(String text) {
        StringBuilder builder = new StringBuilder();
        int middle = 0;
        if (text3IsBold) {
            builder.insert(middle, format.get("bold"));
            middle += 3;
        }
        if (text3IsItalic) {
            builder.insert(middle, format.get("italic"));
            middle += 3;
        }
        if (text3IsUnderline) {
            builder.insert(middle, format.get("underline"));
            middle += 3;
        }
        builder.insert(middle, text);
        return builder.toString();
    }


    private ActionMode mActionMode;

    private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

        private MenuItem picture1Opt;
        private MenuItem picture2Opt;
        private MenuItem picture3Opt;

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            picture1Opt = menu.add(0, 1, 1, "Obrazek 1");
            picture1Opt.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
            picture1Opt.setIcon(R.drawable.a);
            picture2Opt = menu.add(0, 2, 2, "Obrazek 2");
            picture2Opt.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
            picture2Opt.setIcon(R.drawable.b);
            picture3Opt = menu.add(0, 3, 3, "Obrazek 3");
            picture3Opt.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
            picture3Opt.setIcon(R.drawable.c);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            int i = menuItem.getOrder();
//            ImageView image = findViewById(R.id.Image2);
            if (i == 1/*menuItem == picture1Opt*/) {
                image.setImageResource(R.drawable.a);
                actionMode.finish();
            } else if (i == 2/*menuItem == picture2Opt*/) {
                image.setImageResource(R.drawable.b);
                actionMode.finish();
            } else if (i == 3/*menuItem == picture3Opt*/) {
                image.setImageResource(R.drawable.c);
                actionMode.finish();
            } else {
                actionMode.finish();
                return false;
            }
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            mActionMode = null;
        }
    };

    private MenuItem item1;
    private MenuItem item2;
    private MenuItem item3;
    private SubMenu subMenu;
    private MenuItem item1Submenu;
    private MenuItem item2Submenu;
    private MenuItem item3Submenu;

    /*
    CONTEXT MENU
     */
    private MenuItem text1Context1;
    private MenuItem text1Context2;
    private MenuItem text1Context3;

    private MenuItem text2Context1;
    private MenuItem text2Context2;
    private MenuItem text2Context3;

    private MenuItem textToFormatContext1;
    private MenuItem textToFormatContext2;
    private MenuItem textToFormatContext3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);

        registerForContextMenu(findViewById(R.id.act3Text1));
        registerForContextMenu(findViewById(R.id.act3Text2));
        registerForContextMenu(findViewById(R.id.activity3textToFormat));
        image = findViewById(R.id.Image2);
        image.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (mActionMode != null) {
                    return false;
                }
                mActionMode = startActionMode(actionModeCallback);
                view.setSelected(true);
                return true;
            }
        });

        text3IsBold = false;
        text3IsItalic = false;
        text3IsUnderline = false;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if (v.getId() == R.id.act3Text1) {
            text1Context1 = menu.add("Tekst 1");
            text1Context1.setIcon(R.drawable.t1);
            text1Context2 = menu.add("Tekst 2");
            text1Context2.setIcon(R.drawable.t2);
            text1Context3 = menu.add("Tekst 3");
            text1Context3.setIcon(R.drawable.t3);
            menu.setHeaderTitle("Wybór tekstu");
        }
        if (v.getId() == R.id.act3Text2) {
            text2Context1 = menu.add("Kolor 1");
            text2Context2 = menu.add("Kolor 2");
            text2Context3 = menu.add("Kolor 3");
            menu.setHeaderTitle("Wybór koloru");
        }
        if (v.getId() == R.id.activity3textToFormat) {
            textToFormatContext1 = menu.add(0, 0, 1,"Pogrub");
            textToFormatContext2 = menu.add(0, 0, 2,"Pochyl");
            textToFormatContext3 = menu.add(0, 0, 3,"Podkresle");
            menu.setGroupCheckable(0, true, false);
            textToFormatContext1.setChecked(text3IsBold);
            textToFormatContext2.setChecked(text3IsItalic);
            textToFormatContext3.setChecked(text3IsUnderline);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        item1 = menu.add("Tekst 1");
        item1.setIcon(R.drawable.t1);
        item1.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        item2 = menu.add("Tekst 2");
        item2.setIcon(R.drawable.t2);
        item3 = menu.add("Tekst 3");
        item3.setIcon(R.drawable.t3);
        subMenu = menu.addSubMenu("Podmenu");
        item1Submenu = subMenu.add("Kolor 1");
        item1Submenu.setIcon(R.drawable.a);
        item1Submenu.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        item2Submenu = subMenu.add("Kolor 2");
        item2Submenu.setIcon(R.drawable.b);
        item2Submenu.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        item3Submenu = subMenu.add("Kolor 3");
        item3Submenu.setIcon(R.drawable.c);
        item3Submenu.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        TextView text1 = findViewById(R.id.act3Text1);
        TextView text2 = findViewById(R.id.act3Text2);
        if (item == item1) {
            text1.setText(R.string.text1Act1);
            text2.setText(R.string.text2Act1);
        } else if (item == item2) {
            text1.setText(R.string.text2Act1);
            text2.setText(R.string.text3Act1);
        } else if (item == item3) {
            text1.setText(R.string.text3Act1);
            text2.setText(R.string.text1Act1);
        } else if (item == item1Submenu) {
            text1.setTextColor(getResources().getColor(R.color.color1));
            text2.setTextColor(getResources().getColor(R.color.color1));
        } else if (item == item2Submenu) {
            text1.setTextColor(getResources().getColor(R.color.color2));
            text2.setTextColor(getResources().getColor(R.color.color2));
        } else if (item == item3Submenu) {
            text1.setTextColor(getResources().getColor(R.color.color3));
            text2.setTextColor(getResources().getColor(R.color.color3));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        TextView text1 = findViewById(R.id.act3Text1);
        TextView text2 = findViewById(R.id.act3Text2);
        TextView text3 = findViewById(R.id.activity3textToFormat);
        if (item == text1Context1) {
            text1.setText(R.string.text1Act1);
        } else if (item == text1Context2) {
            text1.setText(R.string.text2Act1);
        } else if (item == text1Context3) {
            text1.setText(R.string.text3Act1);
        } else if (item == text2Context1) {
            text2.setTextColor(getResources().getColor(R.color.color1));
        } else if (item == text2Context2) {
            text2.setTextColor(getResources().getColor(R.color.color2));
        } else if (item == text2Context3) {
            text2.setTextColor(getResources().getColor(R.color.color3));
        } else if (item == textToFormatContext1) {
            if (item.isChecked()) {
                item.setChecked(false);
                text3IsBold = false;
            } else {
                item.setChecked(true);
                text3IsBold = true;
            }
            text3.setText(Html.fromHtml(formatText(text3.getText().toString())));
        } else if (item == textToFormatContext2) {
            if (item.isChecked()) {
                item.setChecked(false);
                text3IsItalic = false;
            } else {
                item.setChecked(true);
                text3IsItalic = true;
            }
            text3.setText(Html.fromHtml(formatText(text3.getText().toString())));
        } else if (item == textToFormatContext3) {
            if (item.isChecked()) {
                item.setChecked(false);
                text3IsUnderline = false;
            } else {
                item.setChecked(true);
                text3IsUnderline = true;
            }
            text3.setText(Html.fromHtml(formatText(text3.getText().toString())));
        }
        return super.onContextItemSelected(item);
    }
}

