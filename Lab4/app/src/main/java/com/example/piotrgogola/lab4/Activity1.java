package com.example.piotrgogola.lab4;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Hashtable;
import java.util.Map;

public class Activity1 extends AppCompatActivity {

    private boolean text3IsUnderline;
    private boolean text3IsBold;
    private boolean text3IsItalic;
    private static Map<String, String> format = new Hashtable<>(3);
    static
    {
        format.put("underline", "<u></u>");
        format.put("bold", "<b></b>");
        format.put("italic", "<i></i>");
    }

    @NonNull
    private String formatText(String text)
    {
        StringBuilder builder = new StringBuilder();
        int middle = 0;
        if(text3IsBold)
        {
            builder.insert(middle, format.get("bold"));
            middle+=3;
        }
        if(text3IsItalic)
        {
            builder.insert(middle, format.get("italic"));
            middle+=3;
        }
        if(text3IsUnderline)
        {
            builder.insert(middle, format.get("underline"));
            middle+=3;
        }
        builder.insert(middle, text);
        return builder.toString();
    }

    private ActionMode mActionMode;

    private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            MenuInflater menuInflater = actionMode.getMenuInflater();
            menuInflater.inflate(R.menu.contextualmenu_activity_1, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            ImageView image = findViewById(R.id.activity1Image);
            switch (menuItem.getItemId()) {
                case R.id.contextual_menu_1_opt1:
                    image.setImageResource(R.drawable.a);
                    actionMode.finish();
                    break;
                case R.id.contextual_menu_1_opt2:
                    image.setImageResource(R.drawable.b);
                    actionMode.finish();
                    break;
                case R.id.contextual_menu_1_opt3:
                    image.setImageResource(R.drawable.c);
                    actionMode.finish();
                    break;
                default:
                    actionMode.finish();
                    return false;
            }

            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            mActionMode = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);
        registerForContextMenu(findViewById(R.id.act1Text1));
        registerForContextMenu(findViewById(R.id.act1Text2));
        registerForContextMenu(findViewById(R.id.activity1textToFormat));
        ImageView image = findViewById(R.id.activity1Image);
        image.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (mActionMode != null) {
                    return false;
                }
                mActionMode = startActionMode(actionModeCallback);
                view.setSelected(true);
                return true;
            }
        });

        text3IsBold = false;
        text3IsItalic = false;
        text3IsUnderline = false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_activity_1, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.act1Text1) {
            menu.setHeaderTitle("Wybór tekstu");
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.contextmenu_activity_11, menu);
        }
        if (v.getId() == R.id.act1Text2) {
            menu.setHeaderTitle("Wybór koloru");
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.contextmenu_activity_12, menu);
        }
        if (v.getId() == R.id.activity1textToFormat) {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.checkablemenu_activity_1, menu);
            menu.getItem(0).setChecked(text3IsBold);
            menu.getItem(1).setChecked(text3IsItalic);
            menu.getItem(2).setChecked(text3IsUnderline);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        TextView text1 = findViewById(R.id.act1Text1);
        TextView text2 = findViewById(R.id.act1Text2);
        switch (item.getItemId()) {
            case R.id.opt1:
                text1.setText(R.string.text1Act1);
                text2.setText(R.string.text2Act1);
                break;

            case R.id.opt2:
                text1.setText(R.string.text2Act1);
                text2.setText(R.string.text3Act1);
                break;

            case R.id.opt3:
                text1.setText(R.string.text3Act1);
                text2.setText(R.string.text1Act1);
                break;

            case R.id.submenuopt1:
                text1.setTextColor(getResources().getColor(R.color.color1));
                text2.setTextColor(getResources().getColor(R.color.color1));
                break;

            case R.id.submenuopt2:
                text1.setTextColor(getResources().getColor(R.color.color2));
                text2.setTextColor(getResources().getColor(R.color.color2));
                break;

            case R.id.submenuopt3:
                text1.setTextColor(getResources().getColor(R.color.color3));
                text2.setTextColor(getResources().getColor(R.color.color3));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        TextView text1 = findViewById(R.id.act1Text1);
        TextView text2 = findViewById(R.id.act1Text2);
        TextView text3 = findViewById(R.id.activity1textToFormat);
        switch (item.getItemId()) {
            case R.id.context_menu_1_opt1:
                text1.setText(R.string.text1Act1);
                break;
            case R.id.context_menu_1_opt2:
                text1.setText(R.string.text2Act1);
                break;
            case R.id.context_menu_1_opt3:
                text1.setText(R.string.text3Act1);
                break;
            case R.id.context_menu_2_opt1:
                text2.setTextColor(getResources().getColor(R.color.color1));
                break;
            case R.id.context_menu_2_opt2:
                text2.setTextColor(getResources().getColor(R.color.color2));
                break;
            case R.id.context_menu_2_opt3:
                text2.setTextColor(getResources().getColor(R.color.color3));
                break;
            case R.id.checkable_menu_1_bold:
                if (item.isChecked()) {
                    item.setChecked(false);
                    text3IsBold = false;
                } else {
                    item.setChecked(true);
                    text3IsBold = true;
                }
                text3.setText(Html.fromHtml(formatText(text3.getText().toString())));
                break;
            case R.id.checkable_menu_1_italic:
                if (item.isChecked()) {
                    item.setChecked(false);
                    text3IsItalic = false;
                } else {
                    item.setChecked(true);
                    text3IsItalic = true;
                }
                text3.setText(Html.fromHtml(formatText(text3.getText().toString())));
                break;
            case R.id.checkable_menu_1_underline:
                if (item.isChecked()) {
                    item.setChecked(false);
                    text3IsUnderline = false;
                } else {
                    item.setChecked(true);
                    text3IsUnderline = true;
                }
                text3.setText(Html.fromHtml(formatText(text3.getText().toString())));
                break;
        }
        return super.onContextItemSelected(item);
    }
}
