package com.example.piotrgogola.lab4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void activity1OnClickButton(View view) {
        Intent intent = new Intent(this, Activity1.class);
        startActivity(intent);
    }

    public void activity2OnClickButton(View view) {
        Intent intent = new Intent(this, Activity2.class);
        startActivity(intent);
    }

    public void activity3OnClickButton(View view) {
        Intent intent = new Intent(this, Activity3.class);
        startActivity(intent);
    }
}
