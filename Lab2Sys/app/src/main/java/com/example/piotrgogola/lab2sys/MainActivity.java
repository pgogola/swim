package com.example.piotrgogola.lab2sys;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void startDialer(View view) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + ((EditText)findViewById(R.id.numer)).getText().toString()));
        startActivity(intent);
    }

    public void startMojaAplikacja(View view) {
        Intent intent = new Intent(Intent.CATEGORY_DEFAULT);
        intent.setAction(Intent.ACTION_RUN);
        if(intent.resolveActivity(getPackageManager()) != null)
        {
            startActivity(intent);
        }
    }
}
