package com.example.piotrgogola.lab5;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ASensor extends AppCompatActivity implements SensorEventListener {

    private SensorManager mSrMgr = null;
    private int mSensorType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asensor);

        mSrMgr = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensorType = getIntent().getIntExtra("sensorType", Sensor.TYPE_LIGHT);

        if (mSensorType == Sensor.TYPE_LIGHT) {
            setTitle(R.string.light_status);
        } else if (mSensorType == Sensor.TYPE_ACCELEROMETER) {
            setTitle(R.string.accel_status);
        }

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        TextView tv = (TextView) findViewById(R.id.txt_data);
        StringBuilder sb = new StringBuilder();
        if (mSensorType == Sensor.TYPE_LIGHT) {
            sb.append("Ambient light level: " + sensorEvent.values[0] + " lux");
        } else if (mSensorType == Sensor.TYPE_ACCELEROMETER) {
            sb.append("X acceleration: " +
                    String.format("%7.4f", sensorEvent.values[0]) + " m/s\u00B2\n" +
                    "Y acceleration: " +
                    String.format("%7.4f", sensorEvent.values[1]) + " m/s\u00B2\n" +
                    "Z acceleration: " +
                    String.format("%7.4f", sensorEvent.values[2]) + " m/s\u00B2\n");
        }
        tv.setText(sb);

        tv = (TextView) findViewById(R.id.txt_status);
        StringBuilder sb2 = new StringBuilder();
        sb2.append("\nAccuracy: ");
        sb2.append(sensorEvent.accuracy == 3 ? "High" :
                (sensorEvent.accuracy == 2? "Medium" : "Low"));
        tv.setText(sb2);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        final Sensor sens = mSrMgr.getSensorList(mSensorType).get(0);
        mSrMgr.registerListener(this, sens, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSrMgr.unregisterListener(this);
    }
}
