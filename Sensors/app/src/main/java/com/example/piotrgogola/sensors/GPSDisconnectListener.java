package com.example.piotrgogola.sensors;

/**
 * Created by Piotr Gogola on 04.05.2018.
 */

public interface GPSDisconnectListener {
    public void disconnectGPS();

}
