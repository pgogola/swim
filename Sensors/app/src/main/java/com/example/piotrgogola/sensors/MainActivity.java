package com.example.piotrgogola.sensors;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity implements GPSDisconnectListener {

    private ASensor aSensorFragment;
    private GPS gpsFragment;

    private FragmentTransaction transaction;

    private static final String TAG_FRAGMENT = "fragment_id";

    private static final String TAG_F11 = "FragmentASensor";
    private static final String TAG_F12 = "FragmentGPS";

    private static final int NONE = 0;
    private static final int ASENSOR = 1;
    private static final int GPS = 2;

    private int currentFragment;

    private View.OnClickListener sensorSelectButton = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            menuOptionSelect(view);
        }
    };
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(TAG_FRAGMENT, currentFragment);
        super.onSaveInstanceState(outState);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState == null) {
            aSensorFragment = new ASensor();
            gpsFragment = new GPS();
            gpsFragment.registerGPSDisconnectListener(this);
            transaction = getSupportFragmentManager().beginTransaction();

            transaction.add(R.id.sensor_kontener, aSensorFragment, TAG_F11);
            transaction.detach(aSensorFragment);
            transaction.add(R.id.sensor_kontener, gpsFragment, TAG_F12);
            transaction.detach(gpsFragment);
            transaction.commit();

            currentFragment = NONE;
        }
        else
        {
            currentFragment = savedInstanceState.getInt(TAG_FRAGMENT);
            aSensorFragment = (ASensor) getSupportFragmentManager().findFragmentByTag(TAG_F11);
            gpsFragment = (GPS) getSupportFragmentManager().findFragmentByTag(TAG_F12);
        }
        findViewById(R.id.zyroskop_button).setOnClickListener(sensorSelectButton);
        findViewById(R.id.akcelerometr_button).setOnClickListener(sensorSelectButton);
        findViewById(R.id.barometr_button).setOnClickListener(sensorSelectButton);
        findViewById(R.id.magnetometr_button).setOnClickListener(sensorSelectButton);
        findViewById(R.id.light_button).setOnClickListener(sensorSelectButton);
        findViewById(R.id.gps_button).setOnClickListener(sensorSelectButton);
        findViewById(R.id.app_button).setOnClickListener(sensorSelectButton);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Check gyroscope sensor
        setButton(R.id.zyroskop_button, Sensor.TYPE_GYROSCOPE);

        //Check accelerometer sensor
        setButton(R.id.akcelerometr_button, Sensor.TYPE_ACCELEROMETER);

        //Check barometer sensor
        setButton(R.id.barometr_button, Sensor.TYPE_PRESSURE);

        //Check magnetometer sensor
        setButton(R.id.magnetometr_button, Sensor.TYPE_MAGNETIC_FIELD);

        //Check light sensor
        setButton(R.id.light_button, Sensor.TYPE_LIGHT);

        final LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);

        boolean enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        Button sensorButton = findViewById(R.id.gps_button);
//        sensorButton.setTextColor(enabled ? Color.GREEN : Color.RED);
        sensorButton.setEnabled(enabled);
    }

    private void setButton(int buttonId, int sensorType) {
        final SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        boolean enabled = !sm.getSensorList(sensorType).isEmpty();
        Button sensorButton = findViewById(buttonId);
//        sensorButton.setText(getString(R.string.light_status) + " " + getString(enabled?R.string.txt_avail : R.string.txt_unavail));
//        sensorButton.setTextColor(enabled ? Color.GREEN : Color.RED);
        sensorButton.setEnabled(enabled);
    }

    void menuOptionSelect(View view) {
        if (view.getId() == R.id.app_button) {
            Intent in = new Intent(this, AppActivity.class);
            startActivity(in);
        } else {
            transaction = getSupportFragmentManager().beginTransaction();
            switch (currentFragment)
            {
                case NONE:
                    break;
                case ASENSOR:
                    transaction.detach(aSensorFragment);
                    break;
                case GPS:
                    transaction.detach(gpsFragment);
                    break;
            }

            if (view.getId() == R.id.gps_button) {
                transaction.attach(gpsFragment);
                transaction.commit();
                currentFragment = GPS;
            } else {
                Bundle args = new Bundle();
                if (view.getId() == R.id.zyroskop_button) {
                    args.putInt("sensorType", Sensor.TYPE_GYROSCOPE);
                    aSensorFragment.setArguments(args);
                    transaction.attach(aSensorFragment);
                } else if (view.getId() == R.id.akcelerometr_button) {
                    args.putInt("sensorType", Sensor.TYPE_ACCELEROMETER);
                    aSensorFragment.setArguments(args);
                    transaction.attach(aSensorFragment);
                } else if (view.getId() == R.id.barometr_button) {
                    args.putInt("sensorType", Sensor.TYPE_PRESSURE);
                    aSensorFragment.setArguments(args);
                    transaction.attach(aSensorFragment);
                } else if (view.getId() == R.id.magnetometr_button) {
                    args.putInt("sensorType", Sensor.TYPE_MAGNETIC_FIELD);
                    aSensorFragment.setArguments(args);
                    transaction.attach(aSensorFragment);
                } else if (view.getId() == R.id.light_button) {
                    args.putInt("sensorType", Sensor.TYPE_LIGHT);
                    aSensorFragment.setArguments(args);
                    transaction.attach(aSensorFragment);
                }
                transaction.commit();
                currentFragment = ASENSOR;
            }
        }
    }

    @Override
    public void disconnectGPS() {
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.detach(gpsFragment);
        transaction.commit();
        currentFragment = NONE;
    }
}
