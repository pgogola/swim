package com.example.piotrgogola.sensors;


import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import static android.content.Context.LOCATION_SERVICE;


/**
 * A simple {@link Fragment} subclass.
 */
public class GPS extends Fragment implements LocationListener {

    private LocationManager mLocMgr = null;

    private GPSDisconnectListener listener;

    public GPS() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_gps, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        mLocMgr = (LocationManager) getContext().getSystemService(LOCATION_SERVICE);
        TextView txt_data = (TextView) getActivity().findViewById(R.id.txt_status_gps);
        txt_data.setText("Czekam na dane GPS");
    }

    @Override
    public void onLocationChanged(Location location) {
        TextView txt_data = (TextView) getActivity().findViewById(R.id.txt_data_gps);
        StringBuilder sb = new StringBuilder();
        sb.append("Altitude: ");
        sb.append(location.getAltitude());
        sb.append("m\nBearing: ");
        sb.append(location.getBearing());
        sb.append("\u00B0\nLatitude: ");
        sb.append(location.getLatitude());
        sb.append("\nLongitude: ");
        sb.append(location.getLongitude());
        sb.append("\nSpeed: ");
        sb.append(location.getSpeed());
        sb.append("m/s");
        txt_data.setText(sb);

        txt_data = (TextView) getActivity().findViewById(R.id.txt_status_gps);
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Accuracy: ");
        sb2.append(location.getAccuracy());
        sb2.append("m");
        txt_data.setText(sb2);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {
        if (LocationManager.GPS_PROVIDER.contentEquals(s)) {
            listener.disconnectGPS();//finish();
        }
    }

    void registerGPSDisconnectListener(GPSDisconnectListener listener) {
        this.listener = listener;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {
            mLocMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, this);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {
            mLocMgr.removeUpdates(this);
        }

    }

}
