package com.example.piotrgogola.lab1;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Button4Activity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.button4activity);
        ((RadioButton)findViewById(R.id.radioButton1activity4)).setChecked(true);
        (findViewById(R.id.activity4toggleButton)).getRootView().setBackgroundColor(Color.BLUE);

        CheckBox toggleButton = (CheckBox) findViewById(R.id.activity4toggleButton);
        final TextView toggleButtonText = (TextView) findViewById(R.id.toggleButtonTextActivity4);
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    String sourceString = "<b>" + "text do pogrubienia" + "</b> ";
                    toggleButtonText.setText(Html.fromHtml(sourceString));
                }
                else
                {
                    String sourceString =  "text do pogrubienia";
                    toggleButtonText.setText(Html.fromHtml(sourceString));;
                }
            }
        });
        toggleButton.setChecked(false);
        toggleButtonText.setText(Html.fromHtml("text do pogrubienia"));

    }

    public void radioButtonOnClick(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.radioButton1activity4:
                if (checked) {
                    view.getRootView().setBackgroundColor(Color.BLUE);
                }
                break;
            case R.id.radioButton2activity4:
                if (checked) {
                    view.getRootView().setBackgroundColor(Color.YELLOW);
                }
                break;
        }
    }

    public void goTo5(View view) {
        final Intent activity5 = new Intent(this, Button5Activity.class);
        startActivity(activity5);
        finish();
    }

    public void activity4Back(View view) {
        onBackPressed();
    }
}
