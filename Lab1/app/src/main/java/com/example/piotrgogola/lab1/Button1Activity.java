package com.example.piotrgogola.lab1;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Button1Activity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.button1activity);
        ToggleButton toggleButton = (ToggleButton) findViewById(R.id.activity3toggleButton);
        final TextView toggleButtonText = (TextView) findViewById(R.id.toggleButtonText);
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    toggleButtonText.setText(R.string.toggleButtonOn);
                }
                else
                {
                    toggleButtonText.setText(R.string.toggleButtonOff);
                }
            }
        });
        toggleButton.setChecked(false);
        ((RadioButton)findViewById(R.id.radioButton1)).setChecked(true);
        ((ImageView) findViewById(R.id.activity3radioButtonPic)).setImageResource(R.drawable.a1);
        toggleButtonText.setText(R.string.toggleButtonOff);
    }

    public void activity1Back(View view) {
        onBackPressed();
    }

    public void activity1Toast(View view) {
        Toast.makeText(getApplicationContext(),
            "Linear View",
                Toast.LENGTH_SHORT).show();
    }

    public void radioButtonOnClick(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        final ImageView radioButtonImageView = (ImageView) findViewById(R.id.activity3radioButtonPic);

        switch (view.getId())
        {
            case R.id.radioButton1:
                if(checked)
                {
                    radioButtonImageView.setImageResource(R.drawable.a1);
                }
                break;
            case R.id.radioButton2:
                if(checked)
                {
                    radioButtonImageView.setImageResource(R.drawable.a2);
                }
                break;
        }
    }
}
