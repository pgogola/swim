package com.example.piotrgogola.lab1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void button1OnClick(View view) {
        final Intent activity1 = new Intent(this, Button1Activity.class);
        startActivity(activity1);
    }

    public void button2OnClick(View view) {
        final Intent activity2 = new Intent(this, Button2Activity.class);
        startActivity(activity2);
    }

    public void button3OnClick(View view) {
        final Intent activity3 = new Intent(this, Button3Activity.class);
        startActivity(activity3);
    }

    public void button4OnClick(View view) {
        final Intent activity4 = new Intent(this, Button4Activity.class);
        startActivity(activity4);
    }

    public void button5OnClick(View view) {
        final Intent activity5 = new Intent(this, Button5Activity.class);
        startActivity(activity5);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (onResume) {
            Toast.makeText(getApplicationContext(), "I am back", Toast.LENGTH_SHORT).show();
        }
        onResume = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        onResume = true;
    }

    private boolean onResume = false;
}
