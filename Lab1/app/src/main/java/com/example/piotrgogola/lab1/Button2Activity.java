package com.example.piotrgogola.lab1;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class Button2Activity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.button2activity);
    }

    public void activity2Back(View view) {
        onBackPressed();
    }

    public void activity2Toast(View view) {
        Toast.makeText(getApplicationContext(),
                "Table layout",
                Toast.LENGTH_SHORT).show();
    }
}
