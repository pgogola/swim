package com.example.piotrgogola.lab1;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class Button3Activity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.button3activity);
    }

    public void scrollAction(View view) {
        Toast.makeText(view.getContext(), "scroll button", Toast.LENGTH_SHORT).show();
    }

    public void activity3Back(View view) {
        onBackPressed();
    }
}
