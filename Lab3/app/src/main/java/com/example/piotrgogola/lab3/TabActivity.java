package com.example.piotrgogola.lab3;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class TabActivity extends AppCompatActivity implements ActionBar.TabListener {

    private Fragment11 f1;
    private Fragment12 f2;

    private ActionBar.Tab tab1;
    private ActionBar.Tab tab2;
    private FragmentTransaction transakcja;
    private final String TAB_ID = "tab_id";

    private static final String TAG_F11 = "Fragment11";
    private static final String TAG_F12 = "Fragment12";

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(TAB_ID, getSupportActionBar().getSelectedNavigationIndex());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);


        if (savedInstanceState == null) {
            f1 = new Fragment11();
            f2 = new Fragment12();

            transakcja = getSupportFragmentManager().beginTransaction();
            transakcja.add(R.id.kontener2, f1, TAG_F11);
            transakcja.detach(f1);
            transakcja.add(R.id.kontener2, f2, TAG_F12);
            transakcja.detach(f2);
            transakcja.commit();
            ActionBar actionBar = getSupportActionBar();
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
            tab1 = actionBar.newTab().setText("Opcja 1");
            tab1.setTabListener(this);
            tab2 = actionBar.newTab().setText("Opcja 2");
            tab2.setTabListener(this);
            actionBar.addTab(tab1, true);
            actionBar.addTab(tab2, false);


        } else {
            ActionBar actionBar = getSupportActionBar();
            f1 = (Fragment11) getSupportFragmentManager().findFragmentByTag(TAG_F11);
            f2 = (Fragment12) getSupportFragmentManager().findFragmentByTag(TAG_F12);
            actionBar.setSelectedNavigationItem(savedInstanceState.getInt(TAB_ID));
        }
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        FragmentTransaction transakcja = getSupportFragmentManager().beginTransaction();
        switch (tab.getPosition()) {
            case 0:
                transakcja.attach(f1);
                break;
            case 1:
                transakcja.attach(f2);
                break;
        }
        transakcja.commit();
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        FragmentTransaction transakcja = getSupportFragmentManager().beginTransaction();
        switch (tab.getPosition()) {
            case 0:
                transakcja.detach(f1);
                break;
            case 1:
                transakcja.detach(f2);
                break;
        }
        transakcja.commit();
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }
}
