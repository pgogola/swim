package com.example.piotrgogola.lab5sensors;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class AppActivity extends /*AppCompat*/Activity implements SensorEventListener, AdapterView.OnItemSelectedListener {

    private SensorManager sensorManager = null;
    private Sensor gyroscope;
    private Sensor accelerometer;
    private Sensor magneticField;
    private Sensor orientation;

    ImageView image;

    private float currentDegreeXY = 0f;
    private float currentDegreeYZ = 0f;

    ImageView imageCompass;
    private float currentCompass = 0f;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magneticField = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        orientation = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);

        image = findViewById(R.id.ah_back);

        imageCompass = findViewById(R.id.compass);

        currentDegreeYZ = image.getTop();


        Spinner spinner = findViewById(R.id.frequency);
        List<String> categories = new ArrayList<String>();
        categories.add("Normal");
        categories.add("Game");
        categories.add("UI");
        categories.add("Fastest");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        spinner.setOnItemSelectedListener(this);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        sensorManager.registerListener(this, gyroscope, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
//        sensorManager.registerListener(this, magneticField, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, orientation, SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            AnimationSet as = new AnimationSet(true);

            float degreeXY = Math.round(Math.atan2(sensorEvent.values[0], sensorEvent.values[1])*180/Math.PI);
            RotateAnimation ra = new RotateAnimation(currentDegreeXY, degreeXY, Animation.RELATIVE_TO_SELF,
                    0.5f, Animation.RELATIVE_TO_SELF,0.5f);
            ra.setDuration(250);
            ra.setFillAfter(true);
            as.addAnimation(ra);
            currentDegreeXY = degreeXY;

            float degreeYZ = Math.round(Math.atan2(sensorEvent.values[2], sensorEvent.values[1])*180/Math.PI*5);
            TranslateAnimation ta = new TranslateAnimation(image.getLeft(),image.getLeft(), currentDegreeYZ, image.getLeft()-degreeYZ );
            ta.setDuration(250);
            ta.setFillAfter(true);
            as.addAnimation(ta);
            currentDegreeYZ = image.getLeft()-degreeYZ;

            image.startAnimation(as);
//        } else if (sensorEvent.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
//        } else if (sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
        } else if (sensorEvent.sensor.getType() == Sensor.TYPE_ORIENTATION) {
            float degree = Math.round(sensorEvent.values[0]);
            RotateAnimation ra = new RotateAnimation(currentCompass, -degree, Animation.RELATIVE_TO_SELF,
                    0.5f, Animation.RELATIVE_TO_SELF,0.5f);
            ra.setDuration(250);
            ra.setFillAfter(true);
            imageCompass.startAnimation(ra);
            currentCompass = -degree;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch(i)
        {
            case 1:
                sensorManager.unregisterListener(this);
                sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
                break;
            case 2:
                sensorManager.unregisterListener(this);
                sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
                break;
            case 3:
                sensorManager.unregisterListener(this);
                sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
                break;

            case 4:
                sensorManager.unregisterListener(this);
                sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_FASTEST);
                break;
        }
        sensorManager.registerListener(this, orientation, SensorManager.SENSOR_DELAY_GAME);

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
