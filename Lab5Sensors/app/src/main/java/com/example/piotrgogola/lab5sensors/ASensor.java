package com.example.piotrgogola.lab5sensors;


import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ASensor extends Fragment implements SensorEventListener, AdapterView.OnItemSelectedListener {

    static final public String frequency[] = {"a", "b"};

    private SensorManager mSrMgr = null;
    private int mSensorType;

    public ASensor() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        mSrMgr = (SensorManager) getActivity().getSystemService(getContext().SENSOR_SERVICE);
        Bundle args = getArguments();
        mSensorType = args.getInt("sensorType");

        View v= inflater.inflate(R.layout.fragment_asensor, container, false);

        return v;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        TextView tv = (TextView) getActivity().findViewById(R.id.txt_data);
        StringBuilder sb = new StringBuilder();
        if (mSensorType == Sensor.TYPE_LIGHT) {
            sb.append("Ambient light level: " + sensorEvent.values[0] + " lux");
        } else if (mSensorType == Sensor.TYPE_GYROSCOPE) {
            sb.append("X axis rotation: " +
                    String.format("%7.4f", sensorEvent.values[0]) + " rad/s\n" +
                    "Y axis rotation: " +
                    String.format("%7.4f", sensorEvent.values[1]) + " rad/s\n" +
                    "Z axis rotation: " +
                    String.format("%7.4f", sensorEvent.values[2]) + " rad/s\n");
        } else if (mSensorType == Sensor.TYPE_ACCELEROMETER) {
            sb.append("X acceleration: " +
                    String.format("%7.4f", sensorEvent.values[0]) + " m/s\u00B2\n" +
                    "Y acceleration: " +
                    String.format("%7.4f", sensorEvent.values[1]) + " m/s\u00B2\n" +
                    "Z acceleration: " +
                    String.format("%7.4f", sensorEvent.values[2]) + " m/s\u00B2\n");
        } else if (mSensorType == Sensor.TYPE_PRESSURE) {
            sb.append("X acceleration: " +
                    String.format("%7.4f", sensorEvent.values[0]) + " hPa\n" +
                    "Y acceleration: " +
                    String.format("%7.4f", sensorEvent.values[1]) + " hPa\n" +
                    "Z acceleration: " +
                    String.format("%7.4f", sensorEvent.values[2]) + " hPa\n");
        }
        else if (mSensorType == Sensor.TYPE_MAGNETIC_FIELD) {
            sb.append("X acceleration: " +
                    String.format("%7.4f", sensorEvent.values[0]) + " \u00B5T\n" +
                    "Y acceleration: " +
                    String.format("%7.4f", sensorEvent.values[1]) + " \u00B5T\n" +
                    "Z acceleration: " +
                    String.format("%7.4f", sensorEvent.values[2]) + " \u00B5T\n");
        }
        tv.setText(sb);

        tv = (TextView) getActivity().findViewById(R.id.txt_status);
        StringBuilder sb2 = new StringBuilder();
        sb2.append("\nAccuracy: ");
        sb2.append(sensorEvent.accuracy == 3 ? "High" :
                (sensorEvent.accuracy == 2 ? "Medium" : "Low"));
        sb2.append("\nName: ");
        sb.append(sensorEvent.sensor.getName());
        sb2.append("\nResolution: ");
        sb.append(sensorEvent.sensor.getResolution());
        sb2.append("\nPower: ");
        sb.append(sensorEvent.sensor.getPower());

        tv.setText(sb2);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public void onResume() {
        super.onResume();
        List<Sensor> s = mSrMgr.getSensorList(mSensorType);
        final Sensor sens = mSrMgr.getSensorList(mSensorType).get(0);
        mSrMgr.registerListener(this, sens, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onPause() {
        super.onPause();
        mSrMgr.unregisterListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
