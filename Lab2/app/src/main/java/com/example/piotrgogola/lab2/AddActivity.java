package com.example.piotrgogola.lab2;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class AddActivity extends AppCompatActivity {
    private ArrayList<TextView> elements = new ArrayList<>();
    private ArrayList<Button> buttons = new ArrayList<>();


    static public enum CAR_COLORS {
        RED("Czerwony"), BLUE("Niebieski"), BLACK("Czarny"), GREEN("Zielony"), WHITE("Bialy"), SILVER("Srebrny");
        private String color;

        CAR_COLORS(String s) {
            color = s;
        }

        @Override
        public String toString() {
            return this.color;
        }
    }

    static public enum ENGINE_CAPACITY {
        CAP_16("1.6l"), CAP_19("1.9l"), CAP_20("2.0l"), CAP_25("2.5l");
        private String poj;

        ENGINE_CAPACITY(String s) {
            poj = s;
        }

        @Override
        public String toString() {
            return this.poj;
        }
    }

    CAR_COLORS carColor;

    ENGINE_CAPACITY enginCapacity;

    RadioGroup radioGroup1, radioGroup2;

    private int imageID;

    RadioGroup.OnCheckedChangeListener listener1 = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            if(i != -1)
            {
                radioGroup2.setOnCheckedChangeListener(null);
                radioGroup2.clearCheck();
                radioGroup2.setOnCheckedChangeListener(listener2);
            }
        }
    };

    RadioGroup.OnCheckedChangeListener listener2 = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            if(i != -1)
            {
                radioGroup1.setOnCheckedChangeListener(null);
                radioGroup1.clearCheck();
                radioGroup1.setOnCheckedChangeListener(listener1);
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        elements.add((TextView) findViewById(R.id.markaSamochoduTV));
        elements.add((TextView) findViewById(R.id.markaSamochodu));
        elements.add((TextView) findViewById(R.id.modelSamochoduTV));
        elements.add((TextView) findViewById(R.id.modelSamochodu));
        elements.add((TextView) findViewById(R.id.colorAuta));
        elements.add((TextView) findViewById(R.id.radioButtonRed));
        elements.add((TextView) findViewById(R.id.radioButtonBlack));
        elements.add((TextView) findViewById(R.id.radioButtonBlue));
        elements.add((TextView) findViewById(R.id.radioButtonGreen));
        elements.add((TextView) findViewById(R.id.radioButtonWhite));
        elements.add((TextView) findViewById(R.id.radioButtonSilver));
        elements.add((TextView) findViewById(R.id.capacityOption));
        elements.add((TextView) findViewById(R.id.capacity16));
        elements.add((TextView) findViewById(R.id.capacity19));
        elements.add((TextView) findViewById(R.id.capacity20));
        elements.add((TextView) findViewById(R.id.capacity25));
        elements.add((TextView) findViewById(R.id.weightOption));
        elements.add((TextView) findViewById(R.id.phoneOption));
        elements.add((TextView) findViewById(R.id.dataOption));
        elements.add((TextView) findViewById(R.id.addButton));
        elements.add((TextView) findViewById(R.id.backFromAddButton));
        elements.add((TextView) findViewById(R.id.date));
        elements.add((TextView) findViewById(R.id.phoneNumber));
        elements.add((TextView) findViewById(R.id.carWeight));

        SharedPreferences sharedPreferences = getSharedPreferences(MainActivity.sharedPreferencesFile, MODE_PRIVATE);
        int textColor = sharedPreferences.getInt("fontColor", DefaultColorValues.dFontColor);
        int backgroundColor = sharedPreferences.getInt("backgroundColor", DefaultColorValues.dBackgroundColor);
        int buttonColor = sharedPreferences.getInt("buttonColor", DefaultColorValues.dButtonColor);

        int fontStyle = sharedPreferences.getInt("fontStyle", Typeface.NORMAL);
        for (TextView x : elements) {
            x.setTextColor(textColor);
            x.setTypeface(null, fontStyle);
        }

        buttons.add((Button) findViewById(R.id.backFromAddButton));
        buttons.add((Button) findViewById(R.id.addButton));
        for (Button x : buttons) {
            x.setBackgroundColor(buttonColor);
        }
        getWindow().setBackgroundDrawable(new ColorDrawable(backgroundColor));


        radioGroup1 = (RadioGroup) findViewById(R.id.carColorRadioGroup1);
        radioGroup2 = (RadioGroup) findViewById(R.id.carColorRadioGroup2);


        radioGroup1.setOnCheckedChangeListener(listener1);
        radioGroup2.setOnCheckedChangeListener(listener2);

        ((RadioButton)findViewById(R.id.radioButtonRed)).setChecked(true);
        carColor = CAR_COLORS.RED;
        imageID = R.drawable.red;
        ((RadioButton)findViewById(R.id.capacity16)).setChecked(true);
        enginCapacity = ENGINE_CAPACITY.CAP_16;

    }

    public void carColorOnClick(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.radioButtonRed:
                carColor = CAR_COLORS.RED;
                imageID = R.drawable.red;
                break;
            case R.id.radioButtonBlue:
                carColor = CAR_COLORS.BLUE;
                imageID = R.drawable.blue;
                break;
            case R.id.radioButtonBlack:
                carColor = CAR_COLORS.BLACK;
                imageID = R.drawable.black;
                break;
            case R.id.radioButtonGreen:
                carColor = CAR_COLORS.GREEN;
                imageID = R.drawable.green;
                break;
            case R.id.radioButtonWhite:
                carColor = CAR_COLORS.WHITE;
                imageID = R.drawable.white;
                break;
            case R.id.radioButtonSilver:
                carColor = CAR_COLORS.SILVER;
                imageID = R.drawable.silver;
                break;
        }
    }

    public void capacityOnClick(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.capacity16:
                enginCapacity = ENGINE_CAPACITY.CAP_16;
                break;
            case R.id.capacity19:
                enginCapacity = ENGINE_CAPACITY.CAP_19;
                break;
            case R.id.capacity20:
                enginCapacity = ENGINE_CAPACITY.CAP_20;
                break;
            case R.id.capacity25:
                enginCapacity = ENGINE_CAPACITY.CAP_25;
                break;
        }
    }

    public void addButtonOnClick(View view) {
        EditText markaSamochodu = (EditText) findViewById(R.id.markaSamochodu);
        EditText modelSamochodu = (EditText) findViewById(R.id.modelSamochodu);
        EditText carWeight = (EditText) findViewById(R.id.carWeight);
        EditText date = (EditText) findViewById(R.id.date);
        EditText phoneNumber = (EditText) findViewById(R.id.phoneNumber);

        if (markaSamochodu.getText().length() == 0 ||
                modelSamochodu.getText().length() == 0 ||
                carWeight.getText().length() == 0 ||
                date.getText().length() == 0 ||
                phoneNumber.getText().length() == 0) {
            Toast.makeText(this, "Wypelnij wszystkie pola", Toast.LENGTH_SHORT).show();
            return;
        }
        Toast.makeText(this, "Dodano pojazd", Toast.LENGTH_SHORT).show();

        MainActivity.records.add(new CarRecord(markaSamochodu.getText().toString() + " "
                + modelSamochodu.getText().toString(),
                phoneNumber.getText().toString(), carWeight.getText().toString(),
                date.getText().toString(), carColor.toString(), enginCapacity.toString(), imageID));
    }

    public void backButtonOnClick(View view) {
        onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }
}
