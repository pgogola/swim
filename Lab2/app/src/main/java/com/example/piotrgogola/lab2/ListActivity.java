package com.example.piotrgogola.lab2;



import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;
public class ListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        final ListView listView = (ListView)findViewById(R.id.listView);
        MyAdapter myAdapter = new MyAdapter(this, R.layout.list_row, MainActivity.records);
        listView.setAdapter(myAdapter);


        SharedPreferences sharedPreferences = getSharedPreferences(MainActivity.sharedPreferencesFile, MODE_PRIVATE);
        getWindow().setBackgroundDrawable(new ColorDrawable(sharedPreferences.getInt("backgroundColor", DefaultColorValues.dBackgroundColor)));
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }



}
