package com.example.piotrgogola.lab2;

/**
 * Created by Piotr Gogola on 19.03.2018.
 */

public class DefaultColorValues {
    public static int dBackgroundColor = 0xffffffff;
    public static int dFontColor = 0xff000000;
    public static int dButtonColor = 0xff00ff00;
}
