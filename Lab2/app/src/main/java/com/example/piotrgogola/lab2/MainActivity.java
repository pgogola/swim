package com.example.piotrgogola.lab2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    static List<CarRecord> records = new ArrayList<>();
    static String nazwaPliku = "moj_plik";
    static String sharedPreferencesFile = "nazwa";

    private ArrayList<TextView> textViews = new ArrayList<>();
    private ArrayList<Button> buttons = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViews.add((TextView) findViewById(R.id.nameText));
        textViews.add((TextView) findViewById(R.id.data1Text));
        textViews.add((TextView) findViewById(R.id.data2Text));

        buttons.add((Button) findViewById(R.id.settingButton));
        buttons.add((Button) findViewById(R.id.addButton));
        buttons.add((Button) findViewById(R.id.listButton));

        SharedPreferences sharedPreferences = getSharedPreferences(sharedPreferencesFile, MODE_PRIVATE);

        int textColor = sharedPreferences.getInt("fontColor", DefaultColorValues.dFontColor);
        int backgroundColor = sharedPreferences.getInt("backgroundColor", DefaultColorValues.dBackgroundColor);
        int buttonColor = sharedPreferences.getInt("buttonColor", DefaultColorValues.dButtonColor);

        int fontStyle = sharedPreferences.getInt("fontStyle", Typeface.NORMAL);
        for (TextView x : textViews) {
            x.setTextColor(textColor);
            x.setTypeface(null, fontStyle);
        }

        for (Button x : buttons) {
            x.setBackgroundColor(buttonColor);
            x.setTextColor(textColor);
        }
        getWindow().setBackgroundDrawable(new ColorDrawable(backgroundColor));


        try {
            FileInputStream os = openFileInput(nazwaPliku);
            ObjectInputStream ois = new ObjectInputStream(os);
            records = (ArrayList<CarRecord>) ois.readObject();
            os.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            records = new ArrayList<>();
        }
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        SharedPreferences sharedPreferences = getSharedPreferences(sharedPreferencesFile, MODE_PRIVATE);

        int textColor = sharedPreferences.getInt("fontColor", DefaultColorValues.dFontColor);
        int backgroundColor = sharedPreferences.getInt("backgroundColor", DefaultColorValues.dBackgroundColor);
        int buttonColor = sharedPreferences.getInt("buttonColor", DefaultColorValues.dButtonColor);

        int fontStyle = sharedPreferences.getInt("fontStyle", Typeface.NORMAL);
        for (TextView x : textViews) {
            x.setTextColor(textColor);
            x.setTypeface(null, fontStyle);
        }

        for (Button x : buttons) {
            x.setBackgroundColor(buttonColor);
            x.setTextColor(textColor);
        }
        getWindow().setBackgroundDrawable(new ColorDrawable(backgroundColor));
    }

    public void settingsOnClick(View view) {
        Intent activity = new Intent(this, SettingsActivity.class);
        startActivityForResult(activity, 1);
    }

    public void addOnClick(View view) {
        final Intent activity = new Intent(this, AddActivity.class);
        startActivity(activity);
    }

    public void listOnClick(View view) {
        final Intent activity = new Intent(this, ListActivity.class);
        startActivity(activity);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (resultCode == Activity.RESULT_OK) {
                    TextView data1 = (TextView) findViewById(R.id.data1Text);
                    data1.setText(data.getStringExtra("data1"));

                    TextView data2 = (TextView) findViewById(R.id.data2Text);
                    data2.setText(data.getStringExtra("data2"));
                }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            FileOutputStream os = openFileOutput(nazwaPliku, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(records);
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
