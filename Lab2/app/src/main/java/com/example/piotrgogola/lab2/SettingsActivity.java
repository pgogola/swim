package com.example.piotrgogola.lab2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class SettingsActivity extends AppCompatActivity {

    private View colorView;
    private SeekBar seekBarR, seekBarG, seekBarB;
    private Spinner spinner;
    private int spinnerSelected;

    private boolean fontBoldChecked;
    private boolean fontItalicChecked;

    private int fontStyle;

    private int radioButtonNumber;

    private int backgroundColor;
    private int fontColor;
    private int buttonColor;

    private SharedPreferences sharedPreferences;

    private ArrayList<RadioButton> radioButtons = new ArrayList<>();
    private ArrayList<TextView> textViews = new ArrayList<>();
    private ArrayList<Button> buttons = new ArrayList<>();
    private ArrayList<CheckBox> checkBoxes = new ArrayList<>();
    private CheckBox boldFont, italicFont;


    private OnSeekBarChangeListener seekBarListener = new OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            int seekR = seekBarR.getProgress();
            int seekG = seekBarG.getProgress();
            int seekB = seekBarB.getProgress();
            int color = 0xff000000 | (seekR << 16) | (seekG << 8) | seekB;
            colorView.setBackgroundColor(color);
            switch (radioButtonNumber) {
                case 1:
                    getWindow().setBackgroundDrawable(new ColorDrawable(color));
                    backgroundColor = color;
                    break;
                case 2:
                    for (RadioButton x : radioButtons)
                        x.setTextColor(color);
                    for (TextView x : textViews)
                        x.setTextColor(color);
                    for (CheckBox x : checkBoxes)
                        x.setTextColor(color);
                    for (Button x : buttons)
                        x.setTextColor(color);
                    fontColor = color;
                    break;
                case 4:
                    for (Button x : buttons) {
                        x.setBackgroundColor(color);
                    }
                    buttonColor = color;
                    break;
            }

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    void setSeekBarValues(int color) {
        seekBarR.setProgress((0x00ff0000 & color) >> 16);
        seekBarG.setProgress((0x0000ff00 & color) >> 8);
        seekBarB.setProgress(0x000000ff & color);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //Inicializacja spinera
        spinner = (Spinner) findViewById(R.id.setting_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.setting_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        //SeekBar
        //R
        seekBarR = (SeekBar) findViewById(R.id.seekBarR);
        seekBarR.setOnSeekBarChangeListener(seekBarListener);

        //G
        seekBarG = (SeekBar) findViewById(R.id.seekBarG);
        seekBarG.setOnSeekBarChangeListener(seekBarListener);

        //B
        seekBarB = (SeekBar) findViewById(R.id.seekBarB);
        seekBarB.setOnSeekBarChangeListener(seekBarListener);

        //podglad koloru
        colorView = findViewById(R.id.colorView);

        //dodaj radioburrons
        radioButtons.add((RadioButton) findViewById(R.id.setting_color_radio_botton1));
        radioButtons.add((RadioButton) findViewById(R.id.setting_color_radio_botton2));
        radioButtons.add((RadioButton) findViewById(R.id.setting_color_radio_botton4));

        //text view
        textViews.add((TextView) findViewById(R.id.settingMainView));
        textViews.add((TextView) findViewById(R.id.editFont));
        textViews.add((TextView) findViewById(R.id.setColorText));
        textViews.add((TextView) findViewById(R.id.Rcolor));
        textViews.add((TextView) findViewById(R.id.Gcolor));
        textViews.add((TextView) findViewById(R.id.Bcolor));

        //buttons
        buttons.add((Button) findViewById(R.id.resetButton));
        buttons.add((Button) findViewById(R.id.saveButton));
        buttons.add((Button) findViewById(R.id.cancelButton));

        //check boxes
        checkBoxes.add(boldFont = (CheckBox) findViewById(R.id.checkbox_bold));
        checkBoxes.add(italicFont = (CheckBox) findViewById(R.id.checkbox_italic));

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                spinnerSelected = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        readFromPreferences();
    }

    public void readFromPreferences() {
        //ustawienia czcionki
        sharedPreferences = getSharedPreferences(MainActivity.sharedPreferencesFile, MODE_PRIVATE);
        fontBoldChecked = sharedPreferences.getBoolean("fontBold", false);
        fontItalicChecked = sharedPreferences.getBoolean("fontItalic", false);
        ((CheckBox) findViewById(R.id.checkbox_bold)).setChecked(fontBoldChecked);
        ((CheckBox) findViewById(R.id.checkbox_italic)).setChecked(fontItalicChecked);

        fontStyle = sharedPreferences.getInt("fontStyle", Typeface.NORMAL);
        for (TextView x : textViews)
            x.setTypeface(null, fontStyle);
        for (RadioButton x : radioButtons)
            x.setTypeface(null, fontStyle);
        for (CheckBox x : checkBoxes)
            x.setTypeface(null, fontStyle);



        //wczytywanie ustawien kolorow
        backgroundColor = sharedPreferences.getInt("backgroundColor", DefaultColorValues.dBackgroundColor);
        getWindow().setBackgroundDrawable(new ColorDrawable(backgroundColor));

        fontColor = sharedPreferences.getInt("fontColor", DefaultColorValues.dFontColor);
        for (RadioButton x : radioButtons)
            x.setTextColor(fontColor);
        for (TextView x : textViews)
            x.setTextColor(fontColor);
        for (CheckBox x : checkBoxes)
            x.setTextColor(fontColor);

        buttonColor = sharedPreferences.getInt("buttonColor", DefaultColorValues.dButtonColor);
        for (Button x : buttons) {
            x.setBackgroundColor(buttonColor);
            x.setTextColor(fontColor);
        }

        //ustawienia radiobutton
        radioButtonNumber = sharedPreferences.getInt("radioButtonNumber", 1);
        if (radioButtonNumber == 1) {
            ((RadioButton) findViewById(R.id.setting_color_radio_botton1)).setChecked(true);
            setSeekBarValues(backgroundColor);
        } else if (radioButtonNumber == 2) {
            ((RadioButton) findViewById(R.id.setting_color_radio_botton2)).setChecked(true);
            setSeekBarValues(fontColor);
        } else if (radioButtonNumber == 4) {
            ((RadioButton) findViewById(R.id.setting_color_radio_botton4)).setChecked(true);
            setSeekBarValues(buttonColor);
        }

        spinnerSelected = sharedPreferences.getInt("spinnerIndex", 0);

        spinner.setSelection(spinnerSelected);
    }

    public void fontOnClicked(View view) {
        boolean checked = ((CheckBox) view).isChecked();

        switch (view.getId()) {
            case R.id.checkbox_bold:
                if (checked)
                    fontBoldChecked = true;
                else
                    fontBoldChecked = false;
                break;
            case R.id.checkbox_italic:
                if (checked)
                    fontItalicChecked = true;
                else
                    fontItalicChecked = false;
                break;
        }


        if(fontBoldChecked && fontItalicChecked)
        {
            fontStyle = Typeface.BOLD_ITALIC;
        }
        else if(fontItalicChecked)
        {
            fontStyle = Typeface.ITALIC;
        }
        else if(fontBoldChecked)
        {
            fontStyle = Typeface.BOLD;
        }
        else
        {
            fontStyle = Typeface.NORMAL;
        }

        for (TextView x : textViews)
            x.setTypeface(null, fontStyle);
        for (RadioButton x : radioButtons)
            x.setTypeface(null, fontStyle);
        for (CheckBox x : checkBoxes)
            x.setTypeface(null, fontStyle);

    }

    public void radioButtonColorOnClick(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.setting_color_radio_botton1:
                if (checked) {
                    radioButtonNumber = 1;
                    setSeekBarValues(backgroundColor);
                }
                break;
            case R.id.setting_color_radio_botton2:
                if (checked) {
                    radioButtonNumber = 2;
                    setSeekBarValues(fontColor);
                }
                break;
            case R.id.setting_color_radio_botton4:
                if (checked) {
                    radioButtonNumber = 4;
                    setSeekBarValues(buttonColor);
                }
                break;
        }
    }

    public void resetButtonOnClick(View view) {
        //ustawienia czcionki
        fontBoldChecked = false;
        fontItalicChecked = false;
        ((CheckBox) findViewById(R.id.checkbox_bold)).setChecked(fontBoldChecked);
        ((CheckBox) findViewById(R.id.checkbox_italic)).setChecked(fontItalicChecked);

        fontStyle = Typeface.NORMAL;
        for (TextView x : textViews)
            x.setTypeface(null, fontStyle);
        for (RadioButton x : radioButtons)
            x.setTypeface(null, fontStyle);
        for (CheckBox x : checkBoxes)
            x.setTypeface(null, fontStyle);

        //wczytywanie ustawien kolorow
        backgroundColor = DefaultColorValues.dBackgroundColor;
        getWindow().setBackgroundDrawable(new ColorDrawable(backgroundColor));

        fontColor = DefaultColorValues.dFontColor;
        for (RadioButton x : radioButtons)
            x.setTextColor(fontColor);
        for (TextView x : textViews)
            x.setTextColor(fontColor);
        for (CheckBox x : checkBoxes)
            x.setTextColor(fontColor);

        buttonColor = DefaultColorValues.dButtonColor;
        for (Button x : buttons) {
            x.setBackgroundColor(buttonColor);
        }
            //ustawienia radiobutton
        radioButtonNumber = 1;
        if (radioButtonNumber == 1) {
            ((RadioButton) findViewById(R.id.setting_color_radio_botton1)).setChecked(true);
            setSeekBarValues(backgroundColor);
        } else if (radioButtonNumber == 2) {
            ((RadioButton) findViewById(R.id.setting_color_radio_botton2)).setChecked(true);
            setSeekBarValues(fontColor);
        } else if (radioButtonNumber == 4) {
            ((RadioButton) findViewById(R.id.setting_color_radio_botton4)).setChecked(true);
            setSeekBarValues(buttonColor);
        }

        spinnerSelected = 0;

        spinner.setSelection(spinnerSelected);
    }

    public void saveButtonOnClick(View view) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("fontBold", fontBoldChecked);
        editor.putBoolean("fontItalic", fontItalicChecked);

        editor.putInt("fontStyle", fontStyle);

        editor.putInt("radioButtonNumber", radioButtonNumber);

        editor.putInt("backgroundColor", backgroundColor);
        editor.putInt("fontColor", fontColor);
        editor.putInt("buttonColor", buttonColor);
        editor.putString("spinnerText", spinner.getSelectedItem().toString());
        editor.putInt("spinnerIndex", spinnerSelected);
        editor.commit();
    }

    public void cancelButtonOnClick(View view) {
        readFromPreferences();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent resultIntent = new Intent();
                resultIntent.putExtra("data1", spinner.getSelectedItem().toString());
                resultIntent.putExtra("data2", "Kolor tla: " + Integer.toHexString(backgroundColor));
                setResult(RESULT_OK, resultIntent);
                finish();
        }
        return true;
    }
}

