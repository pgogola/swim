package com.example.piotrgogola.lab2;


import java.io.Serializable;

public class CarRecord implements Serializable{
    private String car;
    private String phoneNumber;
    private String carWeight;
    private String date;
    private String color;
    private String capacity;
    private int imageID;

    public CarRecord(String car, String phoneNumber, String carWeight, String date, String color, String capacity, int id) {
        this.imageID = id;
        this.car = car;
        this.phoneNumber = phoneNumber;
        this.carWeight = carWeight;
        this.date = date;
        this.color = color;
        this.capacity = capacity;
    }

    public String getCar() {
        return car;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getCarWeight() {
        return carWeight;
    }

    public String getDate() {
        return date;
    }

    public String getColor() {
        return color;
    }

    public String getCapacity() {
        return capacity;
    }

    public int getImageID() {
        return imageID;
    }

}
