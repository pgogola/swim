package com.example.piotrgogola.lab2;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class MyAdapter extends BaseAdapter {

    private int layoutResource;

    private List<CarRecord> list;

    private Context context;

    public class CarRecordTypes {
        public TextView car;
        public TextView phoneNumber;
        public TextView carWeight;
        public TextView date;
        public TextView color;
        public TextView capacity;
        public ImageView imageID;
        public ImageView capacityView;
    }


    public MyAdapter(Context context, int layoutResource, List<CarRecord> carRecordList) {
        super();
        this.context = context;
        list = carRecordList;
        this.layoutResource = layoutResource;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            view = layoutInflater.inflate(layoutResource, null);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ListRowInfo.class);
                CarRecord info = (CarRecord) getItem(position);
                intent.putExtra("auto", info.getCar());
                intent.putExtra("weight", info.getCarWeight());
                intent.putExtra("capacity", info.getCapacity());
                intent.putExtra("color",info.getColor());
                intent.putExtra("date", info.getDate());
                intent.putExtra("phone", info.getPhoneNumber());
                context.startActivity(intent);
            }
        });

        view.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View view) {
                view.setTag(null);
                list.remove(position);
                notifyDataSetChanged();
                return true;
            }
        });

        CarRecord carRecord = (CarRecord) getItem(position);
        CarRecordTypes carRecordTypes;
        if (carRecord != null) {
            carRecordTypes = new CarRecordTypes();
            carRecordTypes.car = (TextView) view.findViewById(R.id.carTextView);
            carRecordTypes.phoneNumber = (TextView) view.findViewById(R.id.phoneTextView);
            carRecordTypes.date = (TextView) view.findViewById(R.id.dateTextView);
            carRecordTypes.capacity = (TextView) view.findViewById(R.id.capacityTextView);
            carRecordTypes.imageID = (ImageView) view.findViewById(R.id.listRowImage);
            carRecordTypes.capacityView = (ImageView) view.findViewById(R.id.capacityView);
            view.setTag(carRecordTypes);
        } else {
            carRecordTypes = (CarRecordTypes) view.getTag();
        }

        carRecordTypes.car.setText(carRecord.getCar());
        carRecordTypes.phoneNumber.setText(carRecord.getPhoneNumber());
        carRecordTypes.date.setText(carRecord.getDate());
        carRecordTypes.capacity.setText(carRecord.getCapacity());
        carRecordTypes.imageID.setImageResource(carRecord.getImageID());

        if(carRecord.getCapacity().equals("1.6l"))
        {
            carRecordTypes.capacityView.setImageResource(R.drawable.star1);
        }
        if(carRecord.getCapacity().equals("1.9l"))
        {
            carRecordTypes.capacityView.setImageResource(R.drawable.star2);
        }
        if(carRecord.getCapacity().equals("2.0l"))
        {
            carRecordTypes.capacityView.setImageResource(R.drawable.star3);
        }
        if(carRecord.getCapacity().equals("2.5l"))
        {
            carRecordTypes.capacityView.setImageResource(R.drawable.star4);
        }

        return view;
    }
}
