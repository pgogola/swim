package com.example.piotrgogola.lab2;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ListRowInfo extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_row_info);
        ((TextView) findViewById(R.id.markaSamochoduInfo)).setText(getIntent().getStringExtra("auto"));

        ((TextView) findViewById(R.id.colorInfo)).setText(getIntent().getStringExtra("color"));

        ((TextView) findViewById(R.id.telefonInfo)).setText(getIntent().getStringExtra("phone"));

        ((TextView) findViewById(R.id.carWeightInfo)).setText(getIntent().getStringExtra("weight"));

        ((TextView) findViewById(R.id.capacityInfo)).setText(getIntent().getStringExtra("capacity"));

        ((TextView) findViewById(R.id.dateInfo)).setText(getIntent().getStringExtra("date"));

    }

    public void backInfoButtonOnClick(View view) {
        onBackPressed();
    }
}
