package com.example.piotrgogola.lab6audio;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;

public class MainActivity extends /*AppCompat*/Activity {

    private EditText name;
    private EditText surname;
    private EditText title;
    private EditText description;

    private Button startButton;
    private Button stopButton;
    private Button listButton;
    private Button deleteButton;
    private Button saveButton;

    private LinkedBlockingQueue<byte[]> buffer;
    private Recorder audioRecorder;
    private AudioProcessing audioProcessing;

    private String startRecordingDate;

    private Thread recorderThread;
    private Thread processingThread;
    private View.OnClickListener buttonsOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.start_button:

                    Log.i("MainActivity", "Start button");
                    startButton.setEnabled(false);
                    saveButton.setEnabled(false);
                    deleteButton.setEnabled(false);
                    listButton.setEnabled(false);
                    stopButton.setEnabled(true);
                    audioProcessing.setProgressBar((ProgressBar) findViewById(R.id.progress_bar));
                    recorderThread = new Thread(audioRecorder);
                    processingThread = new Thread(audioProcessing);
                    audioRecorder.startRecording();
                    audioProcessing.startProcessing();
                    recorderThread.start();
                    processingThread.start();

                    break;
                case R.id.stop_button:
                    startButton.setEnabled(true);
                    saveButton.setEnabled(true);
                    deleteButton.setEnabled(true);
                    listButton.setEnabled(true);
                    stopButton.setEnabled(false);
                    audioRecorder.stopRecording();
                    audioProcessing.stopProcessing();
                    break;
                case R.id.zapisz_button:
                    if (name.getText().length() == 0 ||
                            surname.getText().length() == 0 ||
                            title.getText().length() == 0 ||
                            description.getText().length() == 0) {
                        Toast.makeText(getApplicationContext(),
                                "Przed zapisaniem notatki dźwiękowej uzupełnij wszystkie pola",
                                Toast.LENGTH_SHORT).show();
                        return;
                    }


                    audioProcessing.saveProcessedAudio(name.getText().toString(),
                            surname.getText().toString(),
                            title.getText().toString(),
                            description.getText().toString(), getBaseContext());
                    break;
                case R.id.skasuj_button:
                    name.setText("");
                    surname.setText("");
                    title.setText("");
                    description.setText("");
                    audioProcessing.removeProcessedAudio();
                    buffer.clear();
                    break;
                default:
                    break;
            }
        }
    };


    public MainActivity() {
        buffer = new LinkedBlockingQueue<>();
        audioProcessing = new AudioProcessing(buffer);
        audioRecorder = new Recorder(buffer);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle(R.string.main_activity_title);

        startButton = findViewById(R.id.start_button);
        startButton.setOnClickListener(buttonsOnClickListener);
        stopButton = findViewById(R.id.stop_button);
        stopButton.setOnClickListener(buttonsOnClickListener);
        saveButton = findViewById(R.id.zapisz_button);
        saveButton.setOnClickListener(buttonsOnClickListener);
        deleteButton = findViewById(R.id.skasuj_button);
        deleteButton.setOnClickListener(buttonsOnClickListener);
        listButton = findViewById(R.id.list_button);

        name = findViewById(R.id.imie_edit_text);
        surname = findViewById(R.id.nazwisko_edit_text);
        title = findViewById(R.id.tytul_edit_text);
        description = findViewById(R.id.opis_edit_text);
        stopButton.setEnabled(false);


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, 0);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }

    }

    public void onStartButtonClick(View view) {
        Intent listActivity = new Intent(this, RecordsList.class);
        startActivity(listActivity);
    }

//    public void onOdtworz(View view) {
//        LinkedList<byte[]> b = audioProcessing.processedAudioSignal;
//        AudioTrack track = new AudioTrack(AudioManager.STREAM_MUSIC, 44100, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT,
//                8192, AudioTrack.MODE_STREAM);
//        track.setVolume(AudioTrack.getMaxVolume() / 2);
//        track.play();
//        while (!b.isEmpty()) {
//            byte[] t = b.remove();
//            track.write(t, 0, t.length);
//        }
//    }
}
