package com.example.piotrgogola.lab6audio;

import android.content.Context;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.attribute.UserDefinedFileAttributeView;
import java.util.ArrayList;


public class RecordRowAdapter extends BaseAdapter {

    static public class LVItem implements Serializable {
        LVItem(String name, String surname, String title, String date, long length, String description, String path) {
            this.name = name;
            this.surname = surname;
            this.title = title;
            this.length = length;
            this.date = date;
            this.path = path;
            this.description = description;
        }

        String name;
        String surname;
        String title;
        long length;
        String description;
        String date;
        String path;
    }

    static private class LVItemGUI {
        TextView title;
        TextView name;
        TextView surname;
        TextView date;
        TextView length;
        CheckBox recordChecked;
    }

    private LayoutInflater inflater;

    private ArrayList<Boolean> checkedElements;

    private ArrayList<LVItem> list;

    Context context;

    public RecordRowAdapter(Context c, int lResource) {
        super();
        list = new ArrayList<>();
        checkedElements = new ArrayList<>();
        context = c;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        File myDir = new File(Environment.getExternalStorageDirectory().toString() + "/saved_files");
        if (myDir.exists()) {
            File[] wavFiles = myDir.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    return pathname.getName().toLowerCase().endsWith(".wav");
                }
            });
            for (File x : wavFiles) {
                try (FileInputStream fi = new FileInputStream(myDir.getPath() + "/m_" + x.getName().substring(0, x.getName().lastIndexOf('.')));
                     ObjectInputStream objectInputStream = new ObjectInputStream(fi)) {
                    LVItem lvItem = (LVItem) objectInputStream.readObject();
                    list.add(lvItem);
                    checkedElements.add(false);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LVItemGUI myItemGUI;
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.record_row_adapter, null);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(context, PlayActivity.class);
                    in.putExtra("name", list.get(position).name);
                    in.putExtra("surname", list.get(position).surname);
                    in.putExtra("description", list.get(position).description);
                    in.putExtra("title", list.get(position).title);
                    in.putExtra("length", list.get(position).length);
                    in.putExtra("date", list.get(position).date);
                    in.putExtra("path", list.get(position).path);
                    context.startActivity(in);
                }
            });

            myItemGUI = new LVItemGUI();
            myItemGUI.length = convertView.findViewById(R.id.record_length);
            myItemGUI.date = convertView.findViewById(R.id.record_date);
            myItemGUI.title = convertView.findViewById(R.id.record_title);
            myItemGUI.name = convertView.findViewById(R.id.record_author_name);
            myItemGUI.surname = convertView.findViewById(R.id.record_author_surname);
            myItemGUI.recordChecked = convertView.findViewById(R.id.record_checked);

            myItemGUI.recordChecked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    checkedElements.set(position, isChecked);
                }
            });

            convertView.setTag(myItemGUI);
        } else {
            myItemGUI = (LVItemGUI) convertView.getTag();
        }

        myItemGUI.title.setText(list.get(position).title);
        myItemGUI.name.setText(list.get(position).name);
        myItemGUI.surname.setText(list.get(position).surname);
        String lenStr = list.get(position).length/60+" min "+list.get(position).length%60 + " sec";
        myItemGUI.length.setText(lenStr);
        myItemGUI.date.setText(list.get(position).date);

        myItemGUI.recordChecked.setChecked(checkedElements.get(position));

        return convertView;
    }

    public void deleteSelected() {
        for (int i = 0; i < list.size(); i++) {
            if (checkedElements.get(i)) {
                File file = new File(list.get(i).path);
                File m_file = new File(file.getParent()+"/m_"+file.getName()
                        .substring(0, file.getName().lastIndexOf('.')));
                if (file.exists()) {
                    file.delete();
                }
                if (m_file.exists()) {
                    m_file.delete();
                }
                checkedElements.remove(i);
                list.remove(i);
                notifyDataSetChanged();
                i--;
            }
        }
    }

    public void mergeSelected() {
        try {
            File file = null;
            FileOutputStream outputStream = null;
            FileInputStream inputStream = null;
            long totalDataLength = 0, totalAudioLen = 0;
            LVItem lvItem = null;
            byte[] buffer = new byte[8192];
            for (int i = 0; i < checkedElements.size(); i++) {
                if (checkedElements.get(i)) {
                    file = new File(list.get(i).path);
                    if (null == outputStream) {
                        inputStream = new FileInputStream(file);
                        inputStream.read(buffer, 0, 44);
                        totalAudioLen = (buffer[40]) | (buffer[41] << 8) | (buffer[42] << 16) | (buffer[43] << 24);
                        inputStream.close();
                        outputStream = new FileOutputStream(file, true);
                        checkedElements.set(i, false);
                        File m_file = new File(file.getParent()+"/m_"+file.getName()
                                .substring(0, file.getName().lastIndexOf('.')));

                        FileInputStream m_inputStream = new FileInputStream(m_file);
                        ObjectInputStream m_objectInput = new ObjectInputStream(m_inputStream);
                        lvItem = (LVItem) m_objectInput.readObject();
                        m_objectInput.close();
                        m_inputStream.close();
                    } else {
                        inputStream = new FileInputStream(file);
                        inputStream.read(buffer, 0, 44);
                        totalAudioLen += (buffer[40]) | (buffer[41] << 8) | (buffer[42] << 16) | (buffer[43] << 24);
                        int len;
                        while ((len = inputStream.read(buffer)) > -1) {
                            outputStream.write(buffer, 0, len);
                        }
                        File m_file = new File(file.getParent()+"/m_"+file.getName()
                                .substring(0, file.getName().lastIndexOf('.')));
                        FileInputStream m_inputStream = new FileInputStream(m_file);
                        ObjectInputStream m_objectInput = new ObjectInputStream(m_inputStream);
                        LVItem tmp = (LVItem) m_objectInput.readObject();
                        lvItem.length+=tmp.length;
                        m_objectInput.close();
                        m_inputStream.close();
                        inputStream.close();

                        if(file.exists())
                        {
                            file.delete();
                        }

                        if(m_file.exists())
                        {
                            m_file.delete();
                        }

                    }
                }
            }
            totalDataLength = totalAudioLen + 36;
            FileChannel fileChannel = outputStream.getChannel();
            fileChannel.write(ByteBuffer.wrap(new byte[]{(byte) (totalDataLength & 0xff),
                    (byte) ((totalDataLength >> 8) & 0xff),
                    (byte) ((totalDataLength >> 16) & 0xff),
                    (byte) ((totalDataLength >> 24) & 0xff)}), 4);
            fileChannel.write(ByteBuffer.wrap(new byte[]{(byte) (totalAudioLen & 0xff),
                    (byte) ((totalAudioLen >> 8) & 0xff),
                    (byte) ((totalAudioLen >> 16) & 0xff),
                    (byte) ((totalAudioLen >> 24) & 0xff)}), 40);
            outputStream.flush();
            outputStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        deleteSelected();
    }
}
