package com.example.piotrgogola.lab6audio;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.provider.MediaStore;
import android.widget.ProgressBar;

import java.util.concurrent.LinkedBlockingQueue;

public class Recorder implements Runnable {


    public Recorder(LinkedBlockingQueue<byte[]> buf) {
        super();
        this.buffer = buf;
        isRecording = false;
    }

    private byte[] getNextBlock() {
        byte[] buf = new byte[BUFFER_SIZE];
        int returnCode = audioRecord.read(buf, 0, buf.length);
        return returnCode >= 0 ? buf : null;
    }

    @Override
    public void run() {
        audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
                44100, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, BUFFER_SIZE);
        if(audioRecord.getState()!=AudioRecord.STATE_INITIALIZED)
        {
            return;
        }
        audioRecord.startRecording();
        try {
            int i = 0;
            while (isRecording) {
                byte[] b = getNextBlock();
                if (null != b)
                {
                    buffer.put(b);
                }

            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            audioRecord.stop();
            audioRecord.release();
            audioRecord = null;
        }
    }

    void startRecording() {
        isRecording = true;
    }

    void stopRecording() {
        isRecording = false;
    }


    static public int BUFFER_SIZE = 8192;
    private AudioRecord audioRecord;
    private LinkedBlockingQueue<byte[]> buffer;
    private boolean isRecording;
}



