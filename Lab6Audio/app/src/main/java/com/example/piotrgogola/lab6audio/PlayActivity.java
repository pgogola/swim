package com.example.piotrgogola.lab6audio;

import android.app.Activity;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;

public class PlayActivity extends Activity {

   private  AudioTrack track;
    private boolean isPlaying = false;

    private String path;
    private float volume = 0.5f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        TextView tmp = findViewById(R.id.show_name);
        tmp.setText(getIntent().getStringExtra("name"));

        tmp = findViewById(R.id.show_surname);
        tmp.setText(getIntent().getStringExtra("surname"));

        tmp = findViewById(R.id.show_date);
        tmp.setText(getIntent().getStringExtra("date"));

        tmp = findViewById(R.id.show_time);
        tmp.setText(getIntent().getStringExtra("time"));

        tmp = findViewById(R.id.show_description);
        tmp.setText(getIntent().getStringExtra("description"));

        tmp = findViewById(R.id.show_title);
        tmp.setText(getIntent().getStringExtra("title"));

        tmp = findViewById(R.id.show_time);
        Long length = getIntent().getLongExtra("length", 0);
        String lenStr = length/60+" min "+length%60 + " sec";
        tmp.setText(lenStr);

        path = getIntent().getStringExtra("path");

        SeekBar sBar = findViewById(R.id.volumeControl);
        sBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                volume = progress/100.0f;
                if(null == track)
                {
                    return;
                }
                track.setVolume(volume);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    public void onStartButtonClick(View view) {
        if (isPlaying) {
            return;
        }
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                isPlaying = true;
                File file = new File(path);
                try {
                    byte[] buffer = new byte[8192];
                    FileInputStream f = new FileInputStream(file);
                    if (44 > f.read(buffer, 0, 44)) {
                        return;
                    }
                    int i;
                    track = new AudioTrack(AudioManager.STREAM_MUSIC,
                            44100,
                            AudioFormat.CHANNEL_OUT_MONO,
                            AudioFormat.ENCODING_PCM_16BIT,
                            8192,
                            AudioTrack.MODE_STREAM);
                    track.setVolume(volume);
                    track.play();
                    while ((i = f.read(buffer)) > -1) {
                        track.write(buffer, 0, i);
                    }
                    track.stop();
                    track.release();
                    f.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                isPlaying = false;
            }
        });
        t.start();
    }
}
