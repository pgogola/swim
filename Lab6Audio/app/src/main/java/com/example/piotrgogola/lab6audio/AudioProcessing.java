package com.example.piotrgogola.lab6audio;

import android.content.Context;
import android.media.AudioFormat;
import android.media.ExifInterface;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ProgressBar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;

public class AudioProcessing implements Runnable {


    public void setProgressBar(ProgressBar pBar) {
        this.pBar = pBar;
    }

    private ProgressBar pBar;

    static public short[] arrayByte2Short(byte[] arr) {
        short[] buffer = new short[arr.length / 2];
        ByteBuffer.wrap(arr).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(buffer);
        return buffer;
    }

    static public int[] arrayByte2Int(byte[] arr) {
        int[] buffer = new int[arr.length / 2];
        for (int i = 0, j = 0; j < arr.length; i++, j += 2) {
            buffer[i] = ((int) (arr[j + 1]) << 8) | (arr[j]);
        }
        return buffer;
    }

    static public byte[] arrayShort2Bytes(short[] arr) {
        byte[] buffer = new byte[arr.length * 2];
        ByteBuffer.wrap(buffer).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().put(arr);
        return buffer;
    }

    private LinkedBlockingQueue<byte[]> rawDataBuffer;
    private boolean isProcessing;
    public LinkedList<byte[]> processedAudioSignal;

    private final short filterThreshold = 10000;


    public AudioProcessing(LinkedBlockingQueue<byte[]> buf) {
        super();
        this.rawDataBuffer = buf;
        isProcessing = false;
        processedAudioSignal = new LinkedList<>();
    }

    public void processAudioBuffer(byte[] buf) {
        int[] arr = arrayByte2Int(buf);
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (max < arr[i]) {
                max = arr[i];
            }
        }
        int L = (int) (20 * Math.log10((double) max / 65536.0));
        Log.i("LOGARYTMICZNIE", "" + L);
        pBar.setProgress(80 + L);
        if (L > -40) {
            processedAudioSignal.addLast(buf);
        }
    }

    void startProcessing() {
        isProcessing = true;
    }

    void stopProcessing() {
        isProcessing = false;
    }

    void removeProcessedAudio() {
        processedAudioSignal.clear();
    }

    void saveProcessedAudio(String name, String surname, String title, String description, Context context) {

        FileOutputStream f;
        FileOutputStream m_f;
        ObjectOutputStream m_fO;

        try {
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/saved_files");
            if (!myDir.exists()) {
                myDir.mkdirs();
            }
            File file = new File(myDir, title + ".wav");
            File metadata = new File(myDir, "m_"+title);
            if (file.exists()) {
                file.delete();
            }
            f = new FileOutputStream(file);
            long timeSec = (long) (processedAudioSignal.size() * (8192/44100.0));
            f.write(prepareWavHeader(processedAudioSignal.size() * 8192));
            while (!processedAudioSignal.isEmpty()) {
                f.write(processedAudioSignal.remove());
            }
            m_f = new FileOutputStream(metadata);
            m_fO =  new ObjectOutputStream(m_f);
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            RecordRowAdapter.LVItem toFile = new RecordRowAdapter.LVItem(name, surname, title,
                    dateFormat.format(Calendar.getInstance().getTime()), timeSec, description, file.getPath());
            m_fO.writeObject(toFile);
            m_fO.close();
            m_f.close();
            f.flush();
            f.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            while (isProcessing) {
                byte[] buf = rawDataBuffer.take();
                processAudioBuffer(buf);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        pBar.setProgress(0);
    }


    public byte[] prepareWavHeader(int pcmDataLengthInBytes) {
        int totalDataLen = pcmDataLengthInBytes + 36;
        int bitsPerSample = 16, channels = 1;
        long totalAudioLen = pcmDataLengthInBytes, longSampleRate = 44100;
        long byteRate = longSampleRate * channels * bitsPerSample / 8;
        byte[] wavHeader = prepareWavFileHeader(bitsPerSample, totalAudioLen,
                totalDataLen, longSampleRate,
                channels, byteRate);
        return wavHeader;
    }


    private byte[] prepareWavFileHeader(int bitsPerSample, long totalAudioLen,
                                        long totalDataLen, long longSampleRate,
                                        int channels, long byteRate) {
        byte[] header = new byte[44];

        header[0] = 'R';
        header[1] = 'I';
        header[2] = 'F';
        header[3] = 'F';
        header[4] = (byte) (totalDataLen & 0xff);
        header[5] = (byte) ((totalDataLen >> 8) & 0xff);
        header[6] = (byte) ((totalDataLen >> 16) & 0xff);
        header[7] = (byte) ((totalDataLen >> 24) & 0xff);
        header[8] = 'W';
        header[9] = 'A';
        header[10] = 'V';
        header[11] = 'E';
        header[12] = 'f';
        header[13] = 'm';
        header[14] = 't';
        header[15] = ' ';
        header[16] = 16;
        header[17] = 0;
        header[18] = 0;
        header[19] = 0;
        header[20] = 1;
        header[21] = 0;
        header[22] = (byte) channels;
        header[23] = 0;
        header[24] = (byte) (longSampleRate & 0xff);
        header[25] = (byte) ((longSampleRate >> 8) & 0xff);
        header[26] = (byte) ((longSampleRate >> 16) & 0xff);
        header[27] = (byte) ((longSampleRate >> 24) & 0xff);
        header[28] = (byte) (byteRate & 0xff);
        header[29] = (byte) ((byteRate >> 8) & 0xff);
        header[30] = (byte) ((byteRate >> 16) & 0xff);
        header[31] = (byte) ((byteRate >> 24) & 0xff);
        header[32] = (byte) (1 * 16 / 8);
        header[33] = 0;
        header[34] = (byte) bitsPerSample;
        header[35] = 0;
        header[36] = 'd';
        header[37] = 'a';
        header[38] = 't';
        header[39] = 'a';
        header[40] = (byte) (totalAudioLen & 0xff);
        header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
        header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
        header[43] = (byte) ((totalAudioLen >> 24) & 0xff);

        return header;
    }
}
